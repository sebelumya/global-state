import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PassengerComponent } from './passenger/passenger.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PassengerDetailComponent } from './passenger-detail/passenger-detail.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import {
  SweetAlert2Module,
  SweetAlert2LoaderService,
} from '@sweetalert2/ngx-sweetalert2';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ButtonModule } from 'primeng/button';
import { StoreModule } from '@ngrx/store';
import { CardModule } from 'primeng/card';
import { MenuItemContent, MenuModule } from 'primeng/menu';
import { MenuItem } from 'primeng/api';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { userReducer } from './store/task/users.reducer';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PassengerComponent,
    AboutUsComponent,
    ContactUsComponent,
    PageNotFoundComponent,
    PassengerDetailComponent,
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SweetAlert2Module.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    ButtonModule,
    StoreModule.forRoot({
      users: userReducer,
    }),
    CardModule,
    MenuModule,
    FontAwesomeModule,
    EffectsModule.forRoot([]),
  ],
  providers: [SweetAlert2LoaderService],

  bootstrap: [AppComponent],
})
export class AppModule {}
