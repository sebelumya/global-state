import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import Swal from 'sweetalert2';
import { PassengerService } from '../services/passenger.service';
import { setUsers } from '../store/task/users.actions';
import { selectAllUser, selectUser } from '../store/task/users.selector';
import { User } from '../store/user-interface';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.css'],
})
export class PassengerComponent implements OnInit {
  isEdit = false;
  data = [];
  inputUser = { name: '', gender: '', city: '', id: '' };
  constructor(
    private passengerService: PassengerService,
    private store: Store
  ) {
    this.users = this.store.select(selectAllUser);
  }
  users: any;

  users$ = this.store.pipe(select(selectUser));

  passengerForm = new FormGroup({
    name: new FormControl(''),
    id: new FormControl(),
    city: new FormControl(''),
  });
  getUsers() {
    console.log('masuk');
    this.passengerService.getUsers();
    // this.store.select(selectUserById());
  }

  ngOnInit() {
    this.getUsers();
  }
  id: number = 0;
  onSubmit() {
    console.log(this.passengerForm.value);
    if (this.isEdit == true) {
      this.isEdit = false;
      this.passengerService
        .deleteUserById(this.passengerForm.value.id)
        .subscribe();
      console.log('1');
      this.passengerService
        .addNewUser(this.passengerForm.value)
        .subscribe((data) => {
          console.log(data);
          Swal.fire({
            title: 'Success!',
            text: 'Data telah diedit',
            icon: 'success',
            toast: true,
            timer: 1500,
            showConfirmButton: false,
          });
        });
      this.getUsers();
      // } else if (
      //   this.users.filter((user: any) => user.id == this.id).length > 0
      // ) {
      //   Swal.fire({
      //     title: 'Error!',
      //     text: 'Data Sudah ada bos!',
      //     icon: 'error',
      //     timer: 1500,
      //     showConfirmButton: false,
      //     toast: true,
      //   });
      // } else {
      //   console.log('4');
      //   this.passengerService
      //     .addNewUser(this.passengerForm.value)
      //     .subscribe((data) => {
      //       console.log(data);
      //       Swal.fire({
      //         title: 'Success!',
      //         icon: 'success',
      //         timer: 1500,
      //         confirmButtonText: 'Cool',
      //         showConfirmButton: false,
      //       });
      //     });
      // // yang dupush bisa juga inputUser
      // }
      this.getUsers();
      this.passengerForm.reset();
    }

    //   deleteFile(id: number) {
    //     this.passengerService.deleteUserById(id).subscribe((data) => {
    //       console.log(data);
    //       Swal.fire({
    //         title: 'Success!',
    //         text: 'Data telah dihapus',
    //         icon: 'success',
    //         timer: 1500,
    //         toast: true,
    //         showConfirmButton: false,
    //       });
    //       this.getUsers();
    //     });
    //   }

    //   edit(id: number) {
    //     this.isEdit = true;
    //     this.passengerService.getUserById(id).subscribe((data: any) => {
    //       console.log(data);
    //       Object.keys(data).map((key) => {
    //         this.passengerForm.get(key)?.setValue(data[key]);
    //       });
    //       // this.passengerForm.get('name')?.setValue(data.name);
    //       // this.passengerForm.get('id')?.setValue(data.id);
    //       // this.passengerForm.get('city')?.setValue(data.city);
    //     });
    //     // console.log(id, '<<id');
    //     // console.log(this.users, '<<user');

    //     // this.doremi = this.users.filter((data: any) => {
    //     //   data.id === id;
    //     //   console.log(data, 'data');

    //     //   console.log(data.id, 'dataid');
    //     //   console.log(typeof data.id, 'dataid');
    //     //   console.log(id, 'id');
    //     //   console.log(typeof id, 'id');
    //     // });
    //     // console.log(this.doremi[0], '<<doremi');

    //     //  = this.doremi[0];
    //   }
    //   // onEdit(id: number) {
    //   //   this.deleteFile(id);
    //   //   this.isEdit = false;
    //   //   this.onSubmit();

    //   //   // alert(`id ${this.data[index].id} telah diedit`);

    //   //   this.passengerForm.reset();
    //   //   // console.log(this.isEdit, '<< isEdit');

    //   //   // Swal.fire({
    //   //   //   title: 'Success!',
    //   //   //   text: 'Data telah diedit',
    //   //   //   icon: 'success',
    //   //   //   toast: true,
    //   //   //   timer: 1500,
    //   //   //   showConfirmButton: false,
    //   //   //   backdrop: true,
    //   //   // });
    //   // }
  }
}
