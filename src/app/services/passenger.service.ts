import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { setUsers } from '../store/task/users.actions';
import { User } from '../store/user-interface';

@Injectable({
  providedIn: 'root',
})
export class PassengerService {
  constructor(private http: HttpClient, private store: Store) {}
  urlApi = 'https://6348c74aa59874146b10a6a5.mockapi.io/api/v1/users';

  getUsers() {
    this.http.get(`${this.urlApi}`).subscribe((data: any) => {
      console.log(data);

      this.store.dispatch(setUsers({ data }));
    });
  }
  // return this.http.post(`${this.urlApi}/users`);
  getUserById(id: number) {
    return this.http.get<User>(`${this.urlApi}/${id}`);
  }
  addNewUser(data: any) {
    return this.http.post<User>(`${this.urlApi}`, data);
  }
  deleteUserById(id: number) {
    return this.http.delete<User>(`${this.urlApi}/${id}`);
  }
  editUserById(data: any, id: number) {
    return this.http.put<User>(`${this.urlApi}/${id}`, data);
  }
}
