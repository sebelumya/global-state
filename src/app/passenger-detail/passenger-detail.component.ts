import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PassengerService } from '../services/passenger.service';
@Component({
  selector: 'app-passenger-detail',
  templateUrl: './passenger-detail.component.html',
  styleUrls: ['./passenger-detail.component.css'],
})
export class PassengerDetailComponent implements OnInit {
  item: any = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private passengerService: PassengerService
  ) {}

  getUserId() {
    const getRoute = this.route.snapshot.paramMap;
    const getId = getRoute.get('id');
    console.log(getId, '>>get iD');
    const id: number = +getId!;
    this.passengerService.getUserById(id).subscribe((data) => {
      this.item.push(data);
      console.log(data), 'data';
    });
  }

  ngOnInit() {
    // console.log(typeof getId, '>>get iD');

    this.getUserId();

    // console.log(this.getUserByid(getId));
  }
}
