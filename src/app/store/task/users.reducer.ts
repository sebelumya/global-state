import { createReducer, on } from '@ngrx/store';
import { User } from '../user-interface';
import { setUsers } from './users.actions';
setUsers;
export const initialState: ReadonlyArray<User> = [];

export interface UserState {
  users: User[];
  user: {};
}
export const userReducer = createReducer(
  initialState,
  on(setUsers, (state, payload: any) => {
    return { ...state, users: payload.data };
  })
);
