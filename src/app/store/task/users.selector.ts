import { createFeatureSelector, createSelector } from '@ngrx/store';
import { User } from '../user-interface';
import { UserState } from './users.reducer';

export const selectUser = createFeatureSelector<UserState>('users');

export const selectAllUser = createSelector(
  selectUser,
  (userState: UserState) => {
    return userState.users;
  }
);

// export const selectUserById = (id: number) =>
//   createSelector(selectUser, (data: User[]) => {
//     let userById = data.filter((e) => e.id === id);
//     if (userById.length == 0) {
//       return null;
//     }
//     return userById[0];
//   });
