import { createAction, props } from '@ngrx/store';
import { User } from '../user-interface';

export const setUsers = createAction(
  '[User] Users set',
  props<{ data: User[] }>()
);
